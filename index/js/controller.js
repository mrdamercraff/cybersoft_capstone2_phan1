const renderProductsList = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
      item;
    contentHTML += `            <div class="card">
        <div class="top-bar">
          <p>${type}</p>
          <em class="stocks">In Stock</em>
        </div>
        <div class="img-container">
          <img
            class="product-img"
            src="${img}"
          />
        </div>
        <div class="details">
          <div class="name-fav">
            <strong class="product-name">${name}</strong>
            <button onclick="" class="heart">
              <i class="fas fa-heart"></i>
            </button>
          </div>
          <div class="wrapper">
            <p>Screen: ${screen}</p>

            <p>Back Camera: ${backCamera}</p>

            <p>frontCamera: ${frontCamera},</p>

            <p>Desc: ${desc}</p>
          </div>
          <div class="purchase">
            <p class="product-price">${price}$</p>
            <span class="btn-add">
              <div>
                <button onclick="addToCart(${id})" class="add-btn">
                  Add<i class="fas fa-chevron-right"></i>
                </button>
              </div>
            </span>
          </div>
        </div>
      </div>
        `;
  });
  document.getElementById("main-card").innerHTML = contentHTML;
};

const renderProductCart = (list) => {
  let contentCART = "";
  let sumPrice = 0;
  let sumQty = 0;
  list.forEach((item) => {
    let { product, quantity } = item;
    contentCART += `
    <tr>
    <td>${product.name}</td>
    <td>${product.price}</td>
    <td>${quantity}</td>
    <td>
    <button onclick='tangCart(${product.id})' class='btn btn-warning'>Tăng</button>
    <button onclick='giamCart(${product.id})' class='btn btn-danger'>Giảm</button>
    <button onclick='xoaCart(${product.id})' class='btn btn-secondary'>Xoá</button>
    </td>
    </tr>`;
    sumQty += quantity / quantity;
    sumPrice += product.price * quantity;
  });
  document.getElementById("tbody-cart").innerHTML = contentCART;
  document.getElementById("tongGiaTien").innerHTML = `Sum: ${sumPrice} $`;
  document.getElementById("qty").innerHTML = sumQty;
};
