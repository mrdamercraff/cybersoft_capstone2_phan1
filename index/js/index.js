const CART = [];
const productList = [];
const json = "json";

let dataJson = localStorage.getItem(json);
if (dataJson !== null) {
  let cartArr = JSON.parse(dataJson);
  for (let i = 0; i < cartArr.length; i++) {
    let item = cartArr[i];
    let itemCart = {
      product: item.product,
      quantity: item.quantity,
    };
    CART.push(itemCart);
  }
  renderProductCart(CART);
}
let fetchAllProducts = (productList) => {
  getAllProducts()
    .then((res) => {
      console.log("Res: ", res);
      productList.push(...res.data);
      renderProductsList(productList);
      console.log(productList[1].id);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchAllProducts(productList);

console.log(productList[1]);
function timDt() {
  getAllProducts()
    .then((res) => {
      console.log("Res: ", res);
      var searchDt = document.getElementById("inputGroupSelect01").value;
      if (searchDt == 0) {
        renderProductsList(res.data);
        return;
      }
      const newArray = res.data.filter((item) => item.type.includes(searchDt));

      renderProductsList(newArray);
    })
    .catch((err) => {
      console.log(err);
    });
}

function addToCart(id) {
  let index = 0;
  for (i = 0; i < productList.length; i++) {
    if (productList[i].id == id) {
      index = i;
    }
  }

  let cartItem = {
    product: {
      id: productList[index].id,
      price: productList[index].price,
      name: productList[index].name,
    },
    quantity: 1,
  };
  if (CART.length == 0) {
    CART.push(cartItem);
  } else {
    for (a = 0; a < CART.length; a++) {
      if (CART[a].product.id == cartItem.product.id) {
        CART[a].quantity = CART[a].quantity + 1;
        luuLocalStorage();
        renderProductCart(CART);
        return CART[a].quantity;
      }
    }
    CART.push(cartItem);
  }
  luuLocalStorage();
  renderProductCart(CART);
}
function tangCart(id) {
  for (i = 0; i < CART.length; i++) {
    if (CART[i].product.id == id) {
      CART[i].quantity = CART[i].quantity + 1;
      luuLocalStorage();
      renderProductCart(CART);
      return CART[i].quantity;
    }
  }
}

function giamCart(id) {
  for (i = 0; i < CART.length; i++) {
    if (CART[i].product.id == id) {
      CART[i].quantity = CART[i].quantity - 1;
      if (CART[i].quantity == 0) {
        CART.splice(i, 1);
        luuLocalStorage();
        renderProductCart(CART);
        return;
      }
      luuLocalStorage();
      renderProductCart(CART);
      return CART[i].quantity;
    }
  }
}
function sideNav(e) {
  let t = document.getElementsByClassName("side-nav")[0];
  let n = document.getElementsByClassName("cover")[0];
  (t.style.right = e ? "0" : "-100%"), (n.style.display = e ? "block" : "none");
}

function luuLocalStorage() {
  let jsonCART = JSON.stringify(CART);
  localStorage.setItem(json, jsonCART);
}

function clearGioHang() {
  CART.length = 0;
  luuLocalStorage();
  renderProductCart(CART);
}
function xoaCart(id) {
  for (i = 0; i < CART.length; i++) {
    if (CART[i].product.id == id) {
      CART.splice(i, 1);
      luuLocalStorage();
      renderProductCart(CART);
      return;
    }
  }
}
