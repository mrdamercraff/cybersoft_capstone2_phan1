let BASE_URL = "https://636a13c4b10125b78fcfe079.mockapi.io/";

let getAllProducts = () => {
  return axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  });
};

